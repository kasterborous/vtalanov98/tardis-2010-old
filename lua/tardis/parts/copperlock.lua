local PART={}
PART.ID = "copperlock"
PART.Name = "copperlock"
PART.Model = "models/doctorwho1200/copper/rewrite/button.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/button.wav" ))
	end
end

TARDIS:AddPart(PART,e)