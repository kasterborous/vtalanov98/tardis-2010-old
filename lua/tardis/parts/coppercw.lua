-- Adds 2010 Copper Curve Walls

local PART={}
PART.ID = "coppercw"
PART.Name = "2010 Copper Curve Walls"
PART.Model = "models/doctorwho1200/copper/rewrite/curvewalls.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)