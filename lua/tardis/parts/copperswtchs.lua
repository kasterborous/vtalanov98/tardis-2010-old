local PART={}
PART.ID = "copperswtchs"
PART.Name = "copperswtchs"
PART.Model = "models/doctorwho1200/copper/rewrite/switches.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/switches.wav" ))
	end
end

TARDIS:AddPart(PART,e)