local PART={}
PART.ID = "copperkeyboard"
PART.Name = "copperkeyboard"
PART.Model = "models/doctorwho1200/copper/rewrite/keyboard.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/keyboard.wav" ))
	end
end

TARDIS:AddPart(PART,e)