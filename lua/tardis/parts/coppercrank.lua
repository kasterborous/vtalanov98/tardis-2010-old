local PART={}
PART.ID = "coppercrank"
PART.Name = "coppercrank"
PART.Model = "models/doctorwho1200/copper/rewrite/crank.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/crank.wav" ))
	end
end

TARDIS:AddPart(PART,e)