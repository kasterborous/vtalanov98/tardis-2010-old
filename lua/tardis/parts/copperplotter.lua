local PART={}
PART.ID = "copperplotter"
PART.Name = "copperplotter"
PART.Model = "models/doctorwho1200/copper/rewrite/plotter.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/plotter.wav" ))
	end
end

TARDIS:AddPart(PART,e)