local PART={}
PART.ID = "coppersilver2"
PART.Name = "coppersilver2"
PART.Model = "models/doctorwho1200/copper/rewrite/silverlever2.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/silverlever2.wav" ))
		self.exterior:ToggleFloat()
	end
end

TARDIS:AddPart(PART,e)