local PART={}
PART.ID = "coppergram"
PART.Name = "coppergram"
PART.Model = "models/doctorwho1200/copper/rewrite/gramophone.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/gramophone.wav" ))
	end
end

TARDIS:AddPart(PART,e)