-- Adds 2010 Copper Levers

local PART={}
PART.ID = "copperlevers"
PART.Name = "2010 Copper Levers"
PART.Model = "models/doctorwho1200/copper/rewrite/levers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)