local PART={}
PART.ID = "coppergoldv"
PART.Name = "coppergoldv"
PART.Model = "models/doctorwho1200/copper/rewrite/goldvalve.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/goldvalve.wav" ))
	end
end

TARDIS:AddPart(PART,e)