-- Adds 2010 Copper Rotor

local PART={}
PART.ID = "copperrotor"
PART.Name = "2010 Copper Rotor"
PART.Model = "models/doctorwho1200/copper/rewrite/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true

if CLIENT then
	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()
		local ext=self.exterior
		if ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex") then
			if self.timerotor.pos==1 then
				self.timerotor.mode=0
			elseif self.timerotor.pos==0 and (ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex")) then
				self.timerotor.mode=1
			end
				
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.55 )
			self:SetPoseParameter( "glass", self.timerotor.pos )
		end
	end
end

TARDIS:AddPart(PART)