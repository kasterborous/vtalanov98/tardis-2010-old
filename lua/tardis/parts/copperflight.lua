local PART={}
PART.ID = "copperflight"
PART.Name = "copperflight"
PART.Model = "models/doctorwho1200/copper/rewrite/flight.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Initialize()
        end
	function PART:Use(activator)
		if activator:IsPlayer() then
		      self:EmitSound( Sound( "doctorwho1200/copper/rewrite/redlever.wav" ))
		      self.exterior:ToggleFlight()
                else return false end
        end
end

TARDIS:AddPart(PART,e)