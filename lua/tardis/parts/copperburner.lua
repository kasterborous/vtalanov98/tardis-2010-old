local PART={}
PART.ID = "copperburner"
PART.Name = "copperburner"
PART.Model = "models/doctorwho1200/copper/rewrite/burner.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/burner.wav" ))
	end
end

TARDIS:AddPart(PART,e)