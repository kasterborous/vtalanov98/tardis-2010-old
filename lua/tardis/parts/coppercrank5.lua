local PART={}
PART.ID = "coppercrank5"
PART.Name = "coppercrank5"
PART.Model = "models/doctorwho1200/copper/rewrite/crank5.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/crank5.wav" ))
	end
end

TARDIS:AddPart(PART,e)