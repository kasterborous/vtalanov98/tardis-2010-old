-- Adds 2010 Copper Roundels

local PART={}
PART.ID = "copperroundels"
PART.Name = "2010 Copper Roundels"
PART.Model = "models/doctorwho1200/copper/rewrite/roundels.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local ext=self.exterior
                mat=Material("models/doctorwho1200/copper/rewrite/roundelsstatic")
		if ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex") then
			  self:SetMaterial("models/doctorwho1200/copper/rewrite/roundels")
                else
                          self:SetMaterial("models/doctorwho1200/copper/rewrite/roundelsstatic")
		end
	end
end

TARDIS:AddPart(PART)