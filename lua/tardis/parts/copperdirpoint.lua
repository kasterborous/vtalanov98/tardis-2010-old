local PART={}
PART.ID = "copperdirpoint"
PART.Name = "copperdirpoint"
PART.Model = "models/doctorwho1200/copper/rewrite/directionalpointer.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/directionalpointer.wav" ))
	end
end

TARDIS:AddPart(PART,e)