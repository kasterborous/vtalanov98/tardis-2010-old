local PART={}
PART.ID = "copperbluev"
PART.Name = "copperbluev"
PART.Model = "models/doctorwho1200/copper/rewrite/bluevalves.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/bluevalves.wav" ))
	end
end

TARDIS:AddPart(PART,e)