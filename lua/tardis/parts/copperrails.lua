-- Adds default rails

local PART={}
PART.ID = "copperrails"
PART.Name = "Copper Rails"
PART.Model = "models/doctorwho1200/copper/rewrite/rails.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)