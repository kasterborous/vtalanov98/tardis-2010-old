local PART={}
PART.ID = "copperfoam"
PART.Name = "copperfoam"
PART.Model = "models/doctorwho1200/copper/rewrite/foammanip.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/foammanip.wav" ))
	end
end

TARDIS:AddPart(PART,e)