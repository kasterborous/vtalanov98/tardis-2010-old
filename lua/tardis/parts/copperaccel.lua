local PART={}
PART.ID = "copperaccel"
PART.Name = "copperaccel"
PART.Model = "models/doctorwho1200/copper/rewrite/accelerator.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 1

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/accelerator.wav" ))
	end
end

TARDIS:AddPart(PART,e)