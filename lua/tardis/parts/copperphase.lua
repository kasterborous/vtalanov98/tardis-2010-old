local PART={}
PART.ID = "copperphase"
PART.Name = "copperphase"
PART.Model = "models/doctorwho1200/copper/rewrite/phaselever.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/phaselever.wav" ))
	end
end

TARDIS:AddPart(PART,e)