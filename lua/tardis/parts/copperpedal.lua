local PART={}
PART.ID = "copperpedal"
PART.Name = "copperpedal"
PART.Model = "models/doctorwho1200/copper/rewrite/pedal.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/pedal.wav" ))
	end
end

TARDIS:AddPart(PART,e)