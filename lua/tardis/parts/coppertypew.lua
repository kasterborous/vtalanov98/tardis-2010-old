local PART={}
PART.ID = "coppertypew"
PART.Name = "coppertypew"
PART.Model = "models/doctorwho1200/copper/rewrite/typewriter.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/typewriter.wav" ))
	end
end

TARDIS:AddPart(PART,e)