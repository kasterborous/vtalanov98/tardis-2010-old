local PART={}
PART.ID = "copperairpump"
PART.Name = "copperairpump"
PART.Model = "models/doctorwho1200/copper/rewrite/airpump.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 1.25

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/airpump.wav" ))
	end
end

TARDIS:AddPart(PART,e)