local PART={}
PART.ID = "copperphysb"
PART.Name = "copperphysb"
PART.Model = "models/doctorwho1200/copper/rewrite/physlock.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/copper/rewrite/redlever.wav" ))
	end
end

TARDIS:AddPart(PART,e)